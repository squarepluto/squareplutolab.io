---
title: AbOUt ME
subtitle: know more about me
date : "2019-07-08T00:54:24+05:30"
author : "Sam"
tags : ["aboutme","introdution"]
image : "img/cover.png"
comments : true comments
share : true            
---
Hello everyone and thanks for visiting my blog.You can call me sam.I love to share my intution and ideas with others but I am shy type. I don't speak much with strangers and I sit infront of my laptop almost all day browsing internet, finding alternatives to what I have and bookmarking them.In a word you can call me [Cybernaut](https://www.merriam-webster.com/dictionary/cybernaut).

Here are the  links to my social media accounts

[Mastodon](https://mastodon.social/@squarepluto)

[Pixelfed](https://pixelfed.social/squarepluto)

[Peertube](https://peertube.social/accounts/squarepluto/videos)

[alternativeto](https://alternativeto.net/user/squarepluto/)
[Blog](http://squarepluto.gq)