---
title: What I Learned This Week(Part 1)
subtitle: Things I learned this week
comments: true
date : "2014-07-11T10:54:24+02:00"
author : "Sam"
tags : ["event","dotScale","sketchnote"]
image : "img/cover-2.png"
comments : true comments
share : true       
menu : ""           
---


This week I installed Kali Linux on my laptop and these are a few things I know and learned this week.
* I you have issues with the nouveau drivers then disable it when you boot to linux.
press e when you get to the grub boot menu and add 
`nouveau.modest=0`
 at the end of the line where it says Linux .Then press F10 to boot to the OS with nouveau disabled.

* You can use apt to install *.deb files
` sudo apt install *.deb `
It will install required dependencies too.


* You can use an alternative mirror if your download speed is low when you update and upgrade
  `sudo nano /etc/apt/sources.list`
 Replace http://http.kali.org/kali in 
`deb http://http.kali.org/kali kali-rolling main non-free contrib`
 with one of the repo from 
 https://http.kali.org/README.mirrorlist


* I recently deleted my apt files in /lib/apt/, then I downloaded the .deb files from the ubuntu packages
https://packages.ubuntu.com/search?keywords=apt-offline-gui
 and installed them
* Tensorflow does not support python 3.7 yet, but kali 2019 comes with python3.7. So you cannot just install tensorflow in kali.