---
title: Installing peertube on Ubuntu 18.04
date : "2019-11-14T13:41:24+05:30"
author : "Sam"
tags : ["Self hosted", "Linux", "Ubuntu", "Peertube"]
share : true          
---


# Installing peertube on Ubuntu 18.04 LTS

## [Dependencies](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/dependencies.md#dependencies)

### 1. On a fresh Ubuntu, as root user, install basic utility programs needed for the installation

```
# apt-get install curl sudo unzip vim
```

### 2. It would be wise to disable root access and to continue this tutorial with a user with sudoers group access

### 3. [Installing Certbot (Nginx on Ubuntu 18.04 LTS -bionic)](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx.html)

1.  SSH into the server
    
    SSH into the server running your HTTP website as a user with sudo privileges.
    
2.  Add Certbot PPA
    
    You'll need to add the Certbot PPA to your list of repositories. To do so, run the following commands on the command line on the machine:
    
    ```
    sudo apt-get updatesudo apt-get install software-properties-commonsudo add-apt-repository universesudo add-apt-repository ppa:certbot/certbotsudo apt-get update
    ```
    
3.  Install Certbot
    
    Run this command on the command line on the machine to install Certbot.
    
    ```
    sudo apt-get install certbot python-certbot-nginx
    ```
    
4.  Choose how you'd like to run Certbot
    
    -   Either get and install your certificates...
        
        Run this command to get a certificate and have Certbot edit your Nginx configuration automatically to serve it, turning on HTTPS access in a single step.
        
        ```
        sudo certbot --nginx
        ```
        
    -   Or, just get a certificate
        
        If you're feeling more conservative and would like to make the changes to your Nginx configuration by hand, run this command.
        
        ```
        sudo certbot certonly --nginx
        ```
        

### 4. [**Node.js v10.x**:](%5Bhttps://github.com/nodesource/distributions/blob/master/README.md#deb%5D(https://github.com/nodesource/distributions/blob/master/README.md#deb)

```
curl -sL [https://deb.nodesource.com/setup_10.x](https://deb.nodesource.com/setup_10.x) | sudo -E bash -sudo apt-get install -y nodejs
```

### 5. [Installing yarn](%5Bhttps://yarnpkg.com/en/docs/install#debian-stable%5D(https://yarnpkg.com/en/docs/install#debian-stable)

On Debian or Ubuntu Linux, you can install Yarn via our Debian package repository. You will first need to configure the repository:

```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
```

On Ubuntu 16.04 or below and Debian Stable, you will also need to configure [the NodeSource repository](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) to get a new enough version of Node.js.

Then you can simply:

```
sudo apt update && sudo apt install yarn
```

**Note**: Ubuntu 17.04 comes with `cmdtest` installed by default. If you’re getting errors from installing `yarn`, you may want to run `sudo apt remove cmdtest` first. Refer to [this](https://github.com/yarnpkg/yarn/issues/2821) for more information.

If using `nvm` you can avoid the `node` installation by doing:

```
sudo apt update && sudo apt install --no-install-recommends yarn
```

### 6. Installing the following software

```
$ sudo apt update$ sudo apt install nginx ffmpeg postgresql postgresql-contrib openssl g++ make redis-server git python-dev$ ffmpeg -version # Should be >= 3.x$ g++ -v # Should be >= 5.x
```

Now that dependencies are installed, before running PeerTube you should start PostgreSQL and Redis:

```
$ sudo systemctl start redis postgresql
```

---

# Installation

Please don't install PeerTube for production on a device behind a low bandwidth connection (example: your ADSL link). If you want information about the appropriate hardware to run PeerTube, please see the [FAQ](https://github.com/Chocobozzz/PeerTube/blob/develop/FAQ.md#should-i-have-a-big-server-to-run-peertube).

### [PeerTube user](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#peertube-user)

### Create a `peertube` user with `/var/www/peertube` home:

```
$ sudo useradd -m -d /var/www/peertube -s /bin/bash -p peertube peertube
```

Set its password:

```
$ sudo passwd peertube
```

**On FreeBSD**

```
$ sudo pw useradd -n peertube -d /var/www/peertube -s /usr/local/bin/bash -m$ sudo passwd peertube
```

or use `adduser` to create it interactively.

### [Database](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#database)

Create the production database and a peertube user inside PostgreSQL:

```
$ sudo -u postgres createuser -P peertube$ sudo -u postgres createdb -O peertube peertube_prod
```

Then enable extensions PeerTube needs:

```
$ sudo -u postgres psql -c "CREATE EXTENSION pg_trgm;" peertube_prod$ sudo -u postgres psql -c "CREATE EXTENSION unaccent;" peertube_prod
```

### [Prepare PeerTube directory](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#prepare-peertube-directory)

Fetch the latest tagged version of Peertube

```
$ VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) && echo "Latest Peertube version is $VERSION"
```

Open the peertube directory, create a few required directories

```
$ cd /var/www/peertube && sudo -u peertube mkdir config storage versions && cd versions
```

Download the latest version of the Peertube client, unzip it and remove the zip

```
$ sudo -u peertube wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"$ sudo -u peertube unzip peertube-${VERSION}.zip && sudo -u peertube rm peertube-${VERSION}.zip
```

Install Peertube:

```
$ cd ../ && sudo -u peertube ln -s versions/peertube-${VERSION} ./peertube-latest$ cd ./peertube-latest && sudo -H -u peertube yarn install --production --pure-lockfile
```

### [PeerTube configuration](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#peertube-configuration)

Copy example configuration:

```
$ cd /var/www/peertube && sudo -u peertube cp peertube-latest/config/production.yaml.example config/production.yaml
```

Then edit the `config/production.yaml` file according to your webserver configuration.

**PeerTube does not support webserver host change**. Keep in mind your domain name is definitive after your first PeerTube start.

### [Webserver](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#webserver)

We only provide official configuration files for Nginx.

Copy the nginx configuration template:

```
$ sudo cp /var/www/peertube/peertube-latest/support/nginx/peertube /etc/nginx/sites-available/peertube
```

Then modify the webserver configuration file. Please pay attention to the `alias` keys of the static locations. It should correspond to the paths of your storage directories (set in the configuration file inside the `storage` key).

```
$ sudo vim /etc/nginx/sites-available/peertube
```

Activate the configuration file:

```
$ sudo ln -s /etc/nginx/sites-available/peertube /etc/nginx/sites-enabled/peertube
```

To generate the certificate for your domain as required to make https work you can use [Let's Encrypt](https://letsencrypt.org/):

```
$ sudo systemctl stop nginx$ sudo vim /etc/nginx/sites-available/peertube # Comment ssl_certificate and ssl_certificate_key lines$ sudo certbot --authenticator standalone --installer nginx --post-hook "systemctl start nginx"$ sudo vim /etc/nginx/sites-available/peertube # Uncomment ssl_certificate and ssl_certificate_key lines$ sudo systemctl reload nginx
```

Remember your certificate will expire in 90 days, and thus needs renewal.

Now you have the certificates you can reload nginx:

```
$ sudo systemctl reload nginx
```

### systemd

If your OS uses systemd, copy the configuration template:

```
$ sudo cp /var/www/peertube/peertube-latest/support/systemd/peertube.service /etc/systemd/system/
```

Update the service file:

```
$ sudo vim /etc/systemd/system/peertube.service
```

Tell systemd to reload its config:

```
$ sudo systemctl daemon-reload
```

If you want to start PeerTube on boot:

```
$ sudo systemctl enable peertube
```

Run:

```
$ sudo systemctl start peertube$ sudo journalctl -feu peertube
```

### Administrator

The administrator password is automatically generated and can be found in the logs. You can set another password with:

```
$ cd /var/www/peertube/peertube-latest && NODE_CONFIG_DIR=/var/www/peertube/config NODE_ENV=production npm run reset-password -- -u root
```

Alternatively you can set the environment variable `PT_INITIAL_ROOT_PASSWORD`, to your own administrator password, although it must be 6 characters or more.

##

##