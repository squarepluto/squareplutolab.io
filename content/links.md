# Websites I vist often
[Protonmail](https://mail.protonmail.com/)

[dev.to](https://dev.to/)

[Packt free learning](https://www.packtpub.com/packt/offers/free-learning)

[Hacker News](https://news.ycombinator.com/)

[hackaday](https://hackaday.com/)

[Alternativeto](https://alternativeto.net/)

[YouTube](http://youtube.com/)

[Pluralsight Free Courses](https://learn.pluralsight.com/resource/free-course/free-weekly-course)

[Mastodon](https://mastodon.social/)

